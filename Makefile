STACK_NAME=hackyeah-cluster
BUCKET_NAME=hackyeah-infrastructure
REGION=us-east-1

.PHONY: deploy

bucket:
	aws s3 mb s3://$(BUCKET_NAME) --region $(REGION)

deploy:
	sam build
	sam package --output-template-file packaged.yml --s3-bucket $(BUCKET_NAME)
	sam deploy \
		--template-file packaged.yml \
		--stack-name $(STACK_NAME) \
		--capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND --region $(REGION) 

destroy:
	aws cloudformation delete-stack --stack-name $(STACK_NAME) --region $(REGION)
	aws s3 rb s3://$(BUCKET_NAME) --force --region $(REGION)
