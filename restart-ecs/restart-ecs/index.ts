/* global module require process */
import { ScheduledHandler } from 'aws-lambda';
import { ECS } from 'aws-sdk';

const ecs: ECS = new ECS();

function any(elements: Boolean[]): Boolean {
  for (const element of elements) {
    if (!!element) return true;
  }
  return false;
}

const handler: ScheduledHandler = async (event) => {
  const ecrImage = `${event.account}.dkr.ecr.${event.region}.amazonaws.com/${event.detail['repository-name']}:${event.detail['image-tag']}`
  console.log({ message: 'Noticed change of ECR tag', ecrImage })
  const { clusterArns } = await ecs.listClusters().promise()
  await Promise.all<void>(clusterArns!.map(async (cluster) => {
    console.log({ message: 'Analyzing cluster', cluster })
    const { serviceArns } = await ecs.listServices({ cluster }).promise()
    await Promise.all<void>(serviceArns!.map(async (serviceArn) => {
      console.log({ message: 'Analyzing service', serviceArn })
      const { services } = await ecs.describeServices({ cluster, services: [serviceArn] }).promise()
      console.log({ message: 'Getting task defintion', taskDefinition: services![0]!.taskDefinition! })
      const params: ECS.Types.DescribeTaskDefinitionRequest = {
        taskDefinition: services![0]!.taskDefinition!
      }
      const { taskDefinition } = await ecs.describeTaskDefinition(params).promise()
      console.log({ taskDefinition })
      if (any(taskDefinition!.containerDefinitions!.map(({ image }) => image === ecrImage))) {
        console.log({ message: 'Restarting service', cluster, serviceArn })
        const params = { service: serviceArn, cluster, forceNewDeployment: true }
        const output = await ecs.updateService(params).promise()
        console.log({ message: 'done', output })
      }
    }))
  }))
}

module.exports.default = handler
